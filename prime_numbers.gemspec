# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'prime_numbers/version'

Gem::Specification.new do |gem|
  gem.name          = "prime_numbers"
  gem.version       = PrimeNumbers::VERSION
  gem.authors       = ["Adit"]
  gem.email         = ["a.saxena@email.it"]
  gem.description   = "Write a program that calculates and prints out a multiplication table of the first 10 calculated prime numbers."
  gem.summary       = "The program must run from the command line and print to screen 1 table.\nAcross the top and down the left side should be the 10 primes, and the body of the table should contain the product of multiplying these numbers."

  gem.homepage      = "https://bitbucket.org/aditsaxena/prime_numbers/src"

  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  
  gem.files         = `git ls-files`.split($/)
  # s.files         = Dir["[A-Z]*[^~]"] + Dir["lib/**/*.rb"] + Dir["spec/*"] + [".gitignore"]
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  
  gem.require_paths = ["lib"]
  
  gem.add_dependency "terminal-table"
  
  gem.add_development_dependency "rake"
  gem.add_development_dependency "rspec"
  gem.add_development_dependency "awesome_print"
  
end

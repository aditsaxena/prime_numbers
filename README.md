# PrimeNumbers

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

    gem 'prime_numbers'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install prime_numbers

## Usage (in terminal)

    > PrimeNumbers.table_product_primes(10)


## WHAT I'VE DONE


1. I've looked around for some existing code ;)
  
  
  - never reinvent from scratch
  
  - 99% of the time somebody has already done something I need to accomplish.
    70% of the time it's the EXACT same request (first 10 prime numbers!)
  
  - I want to learn to "copy the good stuff" and "enhance"
  
  - chances are you get stuff done first and well tested too


2. Once I found some good piece of code I spend my precious time to:

  a) test it's copy-pasted code behaviour
  b) encapsulate into organized structure
  c) package the whole stuff and ship for good

  enjoy!



## SOME NOTES

Just to add something of my own I've added some code extending functionality:

  - monkey patching is considered a bad practice if done wildly
  - better do it "with style", I've tried to implement new beta function "refine"



## REFS

- first N prime numbers: z5h from http://stackoverflow.com/questions/11673968/how-do-i-generate-the-first-n-prime-numbers
- how to monkey patch (BETA): http://www.sitepoint.com/a-look-at-ruby-2-0/
- build a gem: http://railscasts.com/episodes/245-new-gem-with-bundler?view=asciicast

require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

# WARNING: this works for short numbers! (takes too much memory)
class PrimeTester
  def self.is_prime?(n)
    ("1" * n) !~ /^1?$|^(11+?)\1+$/
  end
end

describe PrimeNumbers do
  
  # A good check implements different calculation methods: regex magic != manual loop
  it "test 100 sample numbers to check if our prime calculator return valid number" do
    
    100.times do |i|
      
      is_prime_regx = PrimeTester.is_prime? i
      is_prime_our_class = PrimeInt::Integer.is_prime?(i)
      
      is_prime_regx.should eq(is_prime_our_class)
  
    end  
    
  end
  
  it "Raise an error for invalid product" do
    PrimeNumbers.first_prime_prods(0).should eq 0
    PrimeNumbers.first_prime_prods(-1).should eq 0
    PrimeNumbers.first_prime_prods(-100).should eq 0
  end
  
  it "Test a multiplication table of the first 10 calculated prime numbers" do
    qty = 10
    
    qty.should be > 1
    
    first_primes = PrimeNumbers.fist_primes(qty)
    first_primes_prods = PrimeNumbers.first_prime_prods(qty)
    
    manual_prod = 1
    
    (qty - 1).times do |i|
      
      manual_prod = manual_prod * first_primes[i]
      manual_prod.should eq first_primes_prods[i][2]
      
    end
  end
  
  it "Prints out a multiplication table of the first 10 calculated prime numbers" do
    puts PrimeNumbers.table_product_primes(10)
  end
  
  it "Benchmarks our performance" do
    require 'prime'
    
    Benchmark.bm(7) do |bm|
      bm.report("Ruby Core Prime:")     { Prime.prime?(20_000) }
      bm.report("Our prime validator:") { PrimeInt::Integer.is_prime?(20_000) }
    end
    
  end
  
  it "this Benchmark compares our prime lister vs prime products" do
    require 'prime'
    
    Benchmark.bm(7) do |bm|
      bm.report("list primes only:")   { PrimeNumbers.fist_primes(20_000) }
      bm.report("list primes and its product:")   { PrimeNumbers.first_prime_prods(20_000) }
    end
    
  end
  
end
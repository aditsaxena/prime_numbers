require "terminal-table"
require "prime_numbers/version"
require "prime_numbers/prime_int"

module PrimeNumbers
  
  def self.fist_primes(n = 10)
    e = Enumerator.new do |els|
      i = 2
      loop do
        els << i
        i = PrimeInt::Integer.next_prime(i)
      end
    end
    e.take n
  end
  
  def self.first_prime_prods(n = 10)
    return 0 unless n > 1
    
    rows = []
    next_prime = 0
    prime_prod = 1
    
    (n - 1).times do |i|
      
      next_prime = PrimeInt::Integer.next_prime(next_prime)
      prime_prod = prime_prod * next_prime
      
      rows << [i, next_prime, prime_prod]
    end
    
    rows
  end
  
  def self.table_product_primes(n = 10)
    # This is just a binding from an external plugin which can be modified / substituted during time
    Terminal::Table.new :headings => ['i', 'Prime', 'Product'], :rows => first_prime_prods(n)
  end
  
end

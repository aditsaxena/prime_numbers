module PrimeInt

  class Integer
    
    def self.is_prime?(num)
      return false if num <= 1
      2.upto(Math.sqrt(num).to_i) do |x|
        return false if num % x == 0
      end
      true
    end
    
    def self.next_prime(num)
      n = num + 1
      n = n + 1 until is_prime?(n)
      n
    end
  end
  
end
